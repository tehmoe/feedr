Name: Mohamed Mohamed
Email: mmohamed9991@gmail.com
Time to complete: 90 mins

Notes:
Added software:

- Redux - Makes the state management much easier, we have a few pieces of state that is all linked to the selected preview Items and having 1 source of truth and not having to pass around the data makes the application much simpler to build and easier to debug and add features
- Reselect - I've used reselect because the dietary summary piece with redux alone would add a lot of logic too either the container/component or have an extra store in redux. Instead we can use reselect and have a custom selector to return the exact data we need. It also makes returning the preview items simplier
- lodash - I've included lodash as it has some useful helper functions that makes the code cleaner

Next steps:

- Break down the summary component (didn't do it cause of time)
- Remove CSS and build styled components
- Added react DND and add support to drag and drop the component
