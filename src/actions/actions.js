import {
  LOAD_ITEMS,
  ADD_PREVIEW_ITEM,
  REMOVE_PREVIEW_ITEM
} from "../constants/constants";
import items from "../items";

export function loadItems(category) {
  return {
    type: LOAD_ITEMS,
    payload: {
      items
    }
  };
}

export function addPreviewItem(itemId) {
  return {
    type: ADD_PREVIEW_ITEM,
    payload: {
      itemId
    }
  };
}
export function removePreviewItem(itemId) {
  return {
    type: REMOVE_PREVIEW_ITEM,
    payload: {
      itemId
    }
  };
}
