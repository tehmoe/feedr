import { createSelector } from "reselect";
import { find, flatten, groupBy, map } from "lodash";

const menuBuilderDomain = state => state.reducer;

const menuItemsSelector = () =>
  createSelector(menuBuilderDomain, substate => substate.items);

const previewItemSelector = () =>
  createSelector(menuBuilderDomain, substate =>
    substate.previewItems.map(previewItem =>
      find(substate.items, item => item.id === previewItem)
    )
  );

const previewItemCounter = () =>
  createSelector(previewItemSelector, substate => substate.length);

const dietarySummary = () =>
  createSelector(menuBuilderDomain, substate => {
    const previewItems = substate.previewItems.map(previewItem =>
      find(substate.items, item => item.id === previewItem)
    );
    const deitary = previewItems.map(item => item.dietaries);
    const dietaryGrouped = groupBy(flatten(deitary));
    const groupedByKey = [];
    map(dietaryGrouped, (item, key) => {
      groupedByKey[key] = item.length;
    });
    return groupedByKey;
  });

export {
  menuItemsSelector,
  previewItemSelector,
  previewItemCounter,
  dietarySummary
};
