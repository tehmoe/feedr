import { combineReducers } from "redux";
import reducer from "./reducers/reducers";

export default combineReducers({
  reducer
});
