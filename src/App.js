import React from "react";
import "./App.css";

import Summary from "./components/Summary/Summary";
import MenuBuilder from "./components/MenuBuilder/MenuBuilder";

export default () => (
  <div className="wrapper">
    <Summary />
    <MenuBuilder />
  </div>
);
