import { union } from "lodash";
import {
  LOAD_ITEMS,
  ADD_PREVIEW_ITEM,
  REMOVE_PREVIEW_ITEM
} from "../constants/constants";

const initialState = { items: [], previewItems: [] };

function menuBuilderReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_ITEMS:
      return {
        ...state,
        items: action.payload.items
      };
    case ADD_PREVIEW_ITEM:
      return {
        ...state,
        previewItems: union(state.previewItems, [action.payload.itemId])
      };
    case REMOVE_PREVIEW_ITEM:
      return {
        ...state,
        previewItems: state.previewItems.filter(
          previewItem => previewItem !== action.payload.itemId
        )
      };
    default:
      return state;
  }
}

export default menuBuilderReducer;
