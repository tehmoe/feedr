import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { previewItemCounter, dietarySummary } from "../selectors/selectors";

const mapStateToProps = createStructuredSelector({
  previewItemCount: previewItemCounter(),
  dietarySummaryInfo: dietarySummary()
});

const SummaryContainer = (WrappedComponent: Function) =>
  connect(
    mapStateToProps,
    null
  )(WrappedComponent);

export default SummaryContainer;
