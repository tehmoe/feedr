import { connect } from "react-redux";
import { menuItemsSelector, previewItemSelector } from "../selectors/selectors";
import {
  loadItems,
  addPreviewItem,
  removePreviewItem
} from "../actions/actions";
import { createStructuredSelector } from "reselect";

const mapStateToProps = createStructuredSelector({
  menuItems: menuItemsSelector(),
  previewItems: previewItemSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    loadItems: option => dispatch(loadItems(option)),
    addPreviewItem: option => dispatch(addPreviewItem(option)),
    removePreviewItem: option => dispatch(removePreviewItem(option))
  };
}

const MenuBuilderContainer = (WrappedComponent: Function) =>
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(WrappedComponent);

export default MenuBuilderContainer;
