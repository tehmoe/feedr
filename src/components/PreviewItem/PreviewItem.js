import React from "react";
import Item from "../Item/Item";

export default ({ previewItem, removePreviewItem }) => (
  <li className="item" key={previewItem.id}>
    <Item item={previewItem} />
    <button
      className="remove-item"
      onClick={() => removePreviewItem(previewItem.id)}
    >
      x
    </button>
  </li>
);
