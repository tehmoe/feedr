import React from "react";

export default ({ item }) => (
  <div>
    <h2>{item.name}</h2>
    <p>
      {item.dietaries.map((dietary, index) => (
        <span className="dietary" key={index}>
          {dietary}
        </span>
      ))}
    </p>
  </div>
);
