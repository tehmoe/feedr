import React from "react";
import Item from "../Item/Item";

const menuItemStyle = {
  cursor: "pointer"
};
export default ({ menuItem, addPreviewItem }) => {
  return (
    <li
      className="item"
      onClick={() => addPreviewItem(menuItem.id)}
      style={menuItemStyle}
    >
      <Item item={menuItem} />
    </li>
  );
};
