import React, { Component } from "react";

import SummaryContainer from "../../containers/SummaryContainer";

class Summary extends Component {
  render() {
    return (
      <div className="menu-summary">
        <div className="container">
          <div className="row">
            <div className="col-6 menu-summary-left">
              <span>{this.props.previewItemCount} items</span>
            </div>
            <div className="col-6 menu-summary-right">
              {Object.keys(this.props.dietarySummaryInfo).map(
                key =>
                  this.props.dietarySummaryInfo[key] ? (
                    <span key={key}>
                      {this.props.dietarySummaryInfo[key]}
                      <span className="dietary">{key}</span>
                    </span>
                  ) : (
                    ""
                  )
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SummaryContainer(Summary);
