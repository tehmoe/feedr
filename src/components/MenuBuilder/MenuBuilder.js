import React, { Component } from "react";

import MenuPreview from "../MenuPreview/MenuPreview";
import MenuItems from "../MenuItems/MenuItems";
import MenuBuilderContainer from "../../containers/MenuBuilderContainer";

class MenuBuilder extends Component {
  componentDidMount() {
    this.props.loadItems();
  }
  render() {
    return (
      <div className="container menu-builder">
        <div className="row">
          <div className="col-4">
            <MenuItems
              items={this.props.menuItems}
              addPreviewItem={this.props.addPreviewItem}
            />
          </div>
          <div className="col-8">
            <MenuPreview
              previewItems={this.props.previewItems}
              removePreviewItem={this.props.removePreviewItem}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default MenuBuilderContainer(MenuBuilder);
