import React from "react";
import PreviewItem from "../PreviewItem/PreviewItem";

export default ({ previewItems, removePreviewItem }) => (
  <div>
    <h2>Menu preview</h2>
    <ul className="menu-preview">
      {previewItems.map(previewItem => (
        <PreviewItem
          previewItem={previewItem}
          removePreviewItem={removePreviewItem}
        />
      ))}
    </ul>
  </div>
);
