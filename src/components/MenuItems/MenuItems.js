import React from "react";
import MenuItem from "../MenuItem/MenuItem";

export default ({ items, addPreviewItem }) => {
  return items ? (
    <ul className="item-picker">
      {items.map(item => (
        <MenuItem
          menuItem={item}
          addPreviewItem={addPreviewItem}
          key={item.id}
        />
      ))}
    </ul>
  ) : (
    ""
  );
};
